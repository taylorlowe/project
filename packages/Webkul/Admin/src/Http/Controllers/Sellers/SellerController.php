<?php

namespace Webkul\Admin\Http\Controllers\Sellers;

use Webkul\Admin\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webkul\Attribute\Models\AttributeOption;
use Carbon\Carbon;

class SellerController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

     /**
     * Create a new controller instance.
     *
     * @param \Webkul\Customer\Repositories\CustomerRepository  $customerRepository
     * @param \Webkul\Customer\Repositories\CustomerGroupRepository  $customerGroupRepository
     * @param \Webkul\Core\Repositories\ChannelRepository  $channelRepository
     */
    public function __construct()
    {
        $this->_config = request('_config');

        $this->middleware('admin');

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $getAttributes = AttributeOption::where('attribute_id', 27)->get();

        if ($request->has('id')) {
            $json = file_get_contents('http://merchant.wezu.ge/public/get/merchant/'.$request->id);

            $data = json_decode($json, true);

            AttributeOption::updateOrCreate([
                'admin_name' => $data['name'],
                'sort_order' => 1,
                'attribute_id' => 27
            ]);
        }
    

        return view($this->_config['view'])->with(['getAttributes' => $getAttributes]);
    }
}