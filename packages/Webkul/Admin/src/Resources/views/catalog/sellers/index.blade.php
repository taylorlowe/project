@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.catalog.products.title') }}
@stop

@section('content')
    <div class="content" style="height: 100%;">
        
        

        <div class="page-content">
            <form method="get">
                <div class="control-group">
                    <input type="text" name="id" class="control">
                </div>

                <button type="submit" class="btn btn-lg btn-primary">Add</button>
            </form>

            <ul style="margin-top: 20px">
                @foreach($getAttributes as $attribute)
                    <li>
                        {{ $attribute->admin_name }}
                    </li>
                @endforeach
            </ul>
        </div>

    </div>
@stop

@push('scripts')
    
@endpush